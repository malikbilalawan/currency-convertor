import React from 'react';

const CurrencyExchangeContext = React.createContext();

export default CurrencyExchangeContext;