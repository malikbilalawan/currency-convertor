
import http from './HttpService'

const url = `https://api.exchangeratesapi.io/latest?base=`

export function getLatestRates(baseCurrency){
return http.get(url + baseCurrency)
}




