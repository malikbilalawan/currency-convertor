import React, {useState, useEffect} from 'react';
import Pockets from '../Pockets/Pockets'
import Converter from '../Converter/Converter'
import TransactionHistory from '../TransactionHistory/TransactionHistory'
import CurrencyExchangeContext from '../../context/CurrencyExchangeContext'
import ErrorModal from '../UI/Modal';
import {getLatestRates} from '../../services/CurrencyService'
import './Landing.css'


const Landing = () => {
  
  const [pockets, setPockets] = useState([])
  const [currencies, setCurrencies] = useState({})
  const [baseCurrency, setBaseCurrency] = useState('USD')
  const [transactionHistory, setTransactionHistory] = useState([])
  const [error, setError] = useState()
 
  useEffect(()=>{
    // Assume in our DB, these are available pockets with balance
    const userPockets = [
                          {name: 'USD', amount: 100},
                          {name: 'EUR', amount: 100},
                          {name: 'GBP', amount: 100}
                        ]
    setPockets(userPockets)
  },[])

useEffect(()=>{
  // Fetch the currency rates(based on base currency)
  async function fetchCurrencies(){
    try{
      const {data: currencies} = await getLatestRates(baseCurrency)
      setCurrencies(currencies)
    }catch(e){
     //log error to DB or any logging service
     setError('Something went wrong!')
   }
}
fetchCurrencies();
// Fetch currency after every 10s
const interval = setInterval(fetchCurrencies, 10000)
return ()=>{
clearInterval(interval)
}
},[baseCurrency])

// Any change in child component regarding base currency, 
// update state accordingly
const handleBaseCurrencyChange =  (currency) =>{
  setBaseCurrency( currency)
}
 

const handleCurrencyTransfer = (data) =>{
 let newPockets = [...pockets]
 // Get the index of base and target pockets
 const transferFromIndex = newPockets.map(p => p.name).indexOf(data.baseCurrencyName)
 const transferToIndex = newPockets.map(p => p.name).indexOf(data.targetCurrencyName)

 // extract both (base & target) objects from array
 const oldBaseObject = newPockets[transferFromIndex]
 const oldTargetObject = newPockets[transferToIndex]

 // Check if sufficient balance is available in transfer account
 
if(oldBaseObject.amount < data.baseCurrencyAmount){
  setError('Insufficient Balance')
  return;
}
  
 // Update objects with updated amounts
 oldBaseObject['amount'] -= parseFloat(data.baseCurrencyAmount);
 oldTargetObject['amount'] += parseFloat(data.targetCurrencyAmount);
 
 // Assign back updated objects to array
 newPockets[transferFromIndex] = oldBaseObject;
 newPockets[transferToIndex] = oldTargetObject
 setPockets(newPockets)

 // Save transaction in history
 setTransactionHistory(prevState => [
  {id: Math.random(),
    baseCurrencyName: data.baseCurrencyName,
    targetCurrencyName: data.targetCurrencyName,
    baseCurrencyAmount: data.baseCurrencyAmount,
    targetCurrencyAmount: data.targetCurrencyAmount
  } ,
 ...prevState
 ]
  )
}


// Close modal
const handleCloseModal = () =>{
  setError(null)
}
// data to be share across child components

   const contextData ={
    handleBaseCurrencyChange,
    handleCurrencyTransfer,
    currencies,
    pockets,
    transactionHistory
  }


  return ( 
    <CurrencyExchangeContext.Provider value={contextData}>
      <div className='App'>
      {error && <ErrorModal onClose={handleCloseModal} type='error' heading='An Erorr Occured!'>{error}</ErrorModal>}
      <Pockets />
      <Converter />
      {transactionHistory.length > 0 &&<TransactionHistory />}
    </div>
    </CurrencyExchangeContext.Provider>
   
   );
}
 
export default Landing;
     