import React from 'react';
import {configure, shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Landing from './Landing'
import Pockets from '../Pockets/Pockets'
import Converter from '../Converter/Converter'



configure({adapter: new Adapter()})

describe('<Landing />', ()=>{
  let wrapper;
  beforeEach(()=>{
      wrapper = shallow(<Landing/>)
  })
   it('should render Pockets component', ()=>{
    
     expect(wrapper.find(Pockets)).toHaveLength(1)
   })
   it('should render Converter component', ()=>{
     expect(wrapper.find(Converter)).toHaveLength(1)
   })
  
})