import React from 'react';
import {configure, shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Pocket from './Pocket'
import Card from '../UI/Card'


configure({adapter: new Adapter()})

describe('<Pocket />', ()=>{
  let wrapper;
  beforeEach(()=>{
    wrapper = shallow(<Pocket amount={100} name='USD'/>)
  })

  it('should display one Card component', ()=>{
    expect(wrapper.find(Card)).toHaveLength(1) 
  })

  it('should render two div. One for Heading and one for Value', ()=>{
    expect(wrapper.find('div')).toHaveLength(2) 
  })

  it('should display heading as USD', ()=>{
    expect(wrapper.find('.primary-heading').text()).toBe('USD') 
  })

  it('should display amount as 100.00', ()=>{
    expect(wrapper.find('.mt-1').text()).toBe('100.00') 
  })

})
    
   
   
    

    
      
   
      
    
   
  