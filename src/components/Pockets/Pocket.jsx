import React from 'react';
import Card from '../UI/Card'
import './Pocket.css'

const Pocket = ({name,amount}) => {

  return ( 
<Card>
<div className='primary-heading'>{name}</div>
<div className='mt-1'>{amount.toFixed(2)}</div>
</Card>
   );
}
 
export default Pocket;