import React,{useContext} from 'react';
import Pocket from './Pocket'
import CurrencyExchangeContext from './../../context/CurrencyExchangeContext';
import './Pockets.css'


const Pockets = () => {
 const data = useContext(CurrencyExchangeContext)
 let pockets;
 if(data.pockets.length > 0){
   pockets = data.pockets.map(p => <Pocket key={p.name} {...p}/>)
 }else{
   pockets = <p>Loading...</p>
 }
  return ( 
    <div className='pockets-container'>
    {pockets}
    </div>
   );
}
 
export default Pockets;