import React, {useContext} from 'react';


export const useCurrencyContext = () => useContext(CurrencyContext)
const currencies = {base:'USD', rates:[]}
let pockets = [{name: 'USD', amount: 100}]
let transactionHistory = []
let handleBaseCurrencyChange = ()=>{}
let handleCurrencyTransfer = ()=>{}

const contextData ={
  handleBaseCurrencyChange,
  handleCurrencyTransfer,
  currencies,
  pockets,
  transactionHistory
}

const CurrencyContext = React.createContext(contextData)

export default CurrencyContext;