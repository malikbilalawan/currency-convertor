import React from 'react';
const RateInfoMessage = ({baseCurrencyName, targetCurrencyName, rate}) => {
  let message;
if(rate){
 message =  <div className="my-3">
1 <strong>{baseCurrencyName}</strong> equals to 
{' '}{rate}{' '} 
<strong>{targetCurrencyName}</strong>
</div>
}else{
  message = <div className="my-3">Loading...</div>
}
  return ( 
    <>
    {message}
    </>
   );
}
 
export default RateInfoMessage;