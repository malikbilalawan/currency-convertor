import React,{useState, useContext, useEffect} from 'react';
import Card from '../UI/Card'
import Modal from '../UI/Modal'
import Input from '../Common/Input'
import Select from '../Common/Select'
import Button from '../Common/Button'
import RateInfoMessage from './RateInfoMessage'
import CurrencyExchangeContext from '../../context/CurrencyExchangeContext'
import './Converter.css'

const Converter = () => {
  const [baseCurrencyName, setBaseCurrencyName] = useState('USD')
  const [targetCurrencyName, setTargetCurrencyName] = useState('EUR')
  const [baseCurrencyAmount, setBaseCurrencyAmount] = useState('')
  const [targetCurrencyAmount, setTargetCurrencyAmount] = useState('')
  // Track conversion. whether base to target or from target to base
  const [trackCurrencyInput, setTrackCurrencyInput] = useState('') 
  const [error, setError] = useState()

  // get all data object passed from parent
  const data = useContext(CurrencyExchangeContext)
  
 useEffect(()=>{
   if(baseCurrencyAmount && data.currencies.rates){
    const converted = parseFloat(baseCurrencyAmount) *
                      parseFloat(data.currencies.rates[targetCurrencyName])
    if(trackCurrencyInput === 'Base'){  
      setTargetCurrencyAmount(parseFloat(converted).toFixed(2))
    }else if(trackCurrencyInput === 'Target'){
      const converted = parseFloat(targetCurrencyAmount) /
      parseFloat(data.currencies.rates[targetCurrencyName])
      setBaseCurrencyAmount(parseFloat(converted).toFixed(2))
    }                         
  }
 },[baseCurrencyName, baseCurrencyAmount,trackCurrencyInput, targetCurrencyName, targetCurrencyAmount, data.currencies.rates])

  
  // helper function to vaidate input
  const validated = (value) =>{
   if(Math.floor(value) === value) return 0;
  //check how many digits after decimal
   if(value.indexOf('.')>= 0){
     const count = value.toString().split(".")[1].length || 0
     if(count > 2 ) return false; 
    }
    // Prevent entering two decimals one after each other
    const reg = /((\d*\.\d+|\d+),?)*(\d*\.\d+|\d+)$/;
    if(!reg.test(value)) return false;
     return true;
  }
    
  // Base currency (transfer) input change handler
    const handleBaseCurrencyInputChange = (event) =>{
    let {value} = event.target;
    if(validated(value)){
      setBaseCurrencyAmount(value)
      setTrackCurrencyInput('Base')
     // Calculate base-to-target currency conversion amount and assign to target 
     // currency input
     if(data.currencies){
      const converted= parseFloat(value) * data.currencies.rates[targetCurrencyName]
      if(!Number.isNaN(parseFloat(converted))){  // Avoid assigning NaN
        setTargetCurrencyAmount(parseFloat(converted).toFixed(2))
      }else{
        setTargetCurrencyAmount('')
      }
    }
    }
  }
  
  const handleTargetCurrencyInputChange = (event) =>{
    let {value} = event.target;
    if(validated(value)){
    setTargetCurrencyAmount(value)
    setTrackCurrencyInput('Target')
    if(data.currencies){
    const converted= parseFloat(value) / data.currencies.rates[targetCurrencyName]
    if(!Number.isNaN(parseFloat(converted))){  // Avoid assigning NaN
      setBaseCurrencyAmount(parseFloat(converted).toFixed(2))
    }else{
      setBaseCurrencyAmount('')
    }
  }
}
}
    
  
// Handle Base currency change (select option)
const handleBaseCurrencyChange = (event) =>{
  const {value} = event.target;
  if(value === targetCurrencyName) {
    setError('Same currency exchange not allowed')
    return 
  }
  // pass currency name to main exchange currency component
  data.handleBaseCurrencyChange(value)
  if(data.currencies ){
    const converted= baseCurrencyAmount * data.currencies.rates[targetCurrencyName]
    setTargetCurrencyAmount(parseFloat(converted).toFixed(2))
   }
   setBaseCurrencyName(value)
  
 }
 
// Handle target currency change (select option)
const handleTargetCurrencyChange = (event) =>{
  const {value} = event.target;
  if(value === baseCurrencyName) {
    setError('Same currency exchange not allowed')
    return 
  }
  if(data.currencies){
    const converted= targetCurrencyAmount / data.currencies.rates[value]
    setTargetCurrencyAmount(parseFloat(converted).toFixed(2))
  }
  setTargetCurrencyName(value)
 
}

// clear state
const handleClearState = () =>{
  setBaseCurrencyAmount('')
  setTargetCurrencyAmount('')
}
const onCurrencyTransfer = (event) =>{
 
  event.preventDefault()
  const paramData = {
    baseCurrencyName,
    baseCurrencyAmount,
    targetCurrencyName,
    targetCurrencyAmount
  }

  if(paramData.baseCurrencyAmount === ''){
    setError('Base currency amount can not be empty')
  }else if(paramData.targetCurrencyAmount === ''){
    setError('Target currency amount can not be empty')
  }
  // pass the values to function which will update pockets on parent component
  data.handleCurrencyTransfer(paramData)
  handleClearState()
}

  
 
// Get exchange rate for rate info message
let exchangeRate;
if(data.currencies.rates){
   exchangeRate = parseFloat(data.currencies.rates[targetCurrencyName]).toFixed(2)
}

const closeModal = () =>{
  setError(null)
}
  return ( 
    <Card>
      {error && <Modal type='error' heading='An error has occuerd!' onClose={closeModal}>{error}</Modal>}
      <RateInfoMessage 
        baseCurrencyName={baseCurrencyName}
        targetCurrencyName={targetCurrencyName}
        rate={exchangeRate}/>
      <form className="currency-exchange-form" onSubmit={onCurrencyTransfer}>

        <div className="form-group">
          <div className='input-container'>
         <Input 
            value={baseCurrencyAmount} 
            onChange={handleBaseCurrencyInputChange} 
            label='Base Currency'
            autoFocus={true}/>
         <span className='action-identifier'>-</span>
           
         <Select 
          value={baseCurrencyName} 
          onChange={handleBaseCurrencyChange} 
          options={data.pockets}/>
          </div>
        </div>
        
        <div className="form-group">
        <div className="input-container">
        <Input 
          value={targetCurrencyAmount} 
          onChange={handleTargetCurrencyInputChange} 
          label='Target Currency'
          autoFocus={false}/>
         <span className='action-identifier'>+</span>
         <Select 
          value={targetCurrencyName} 
          onChange={handleTargetCurrencyChange} 
          options={data.pockets}/>
        </div>
        </div>
         
        <Button cssClass='btn btn-primary' label='Transfer'/>
       
      </form>
    </Card>
   );
}
export default Converter;
 
        
     
          
        