import React,{useContext} from 'react';
import Card from '../UI/Card'
import CurrencyExchangeContext from './../../context/CurrencyExchangeContext';
import './TransactionHistory.css'

const TransactionHistory = () => {

  const data = useContext(CurrencyExchangeContext)

  let contents;
  if(data.transactionHistory.length > 0){
      contents =   <ul className='no-style'>
      {data.transactionHistory.map(h => 
        <li key={h.id} className='list-item'>
    <span className='mr-2'>
    {h.baseCurrencyAmount} {h.baseCurrencyName} transferred to {h.transferTo} </span>
      (<span className='arrow-up'></span> {h.targetCurrencyAmount} {h.targetCurrencyName}
      
     {'   '}, <span className='arrow-down'></span> {h.baseCurrencyAmount} {h.baseCurrencyName})
     
          </li>
        )}
    </ul>
  }
  return ( 
    <Card>
      {contents}
    </Card>
   );
}
 
export default TransactionHistory;