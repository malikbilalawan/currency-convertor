import React from 'react';
const Select = ({value, onChange, options}) => {
  return (  
    <select value={value} onChange={onChange} className='form-control'>
     {options.map(val => 
      <option value={val.name} key={val.name}>{val.name}</option>
      )}
    </select>
  );
}
 
export default Select;