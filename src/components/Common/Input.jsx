import React from 'react';
const Input = ({value, onChange, label, autoFocus}) => {
  return ( 
    <input 
    type="number" 
    min="0"
    step=".01"
    autoFocus={autoFocus}
    className="form-control mr-2" 
    value={value}
    onChange={onChange}
    placeholder = {label}
    autoComplete="off"
    />
   );
}
export default Input;
    
    
 