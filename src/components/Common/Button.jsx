import React from 'react';
const Button = ({cssClass, label, onClick}) => {
  return ( 
  <button className={cssClass} onClick={onClick}>{label}</button>
   );
}
export default Button;
 