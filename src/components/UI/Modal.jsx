import React from 'react';
import Button from '../Common/Button'
import './Modal.css';

const Modal = props => {

  return (
    <React.Fragment>
      <div className="backdrop" onClick={props.onClose} />
      <div className="modal">
       <h2 className={`${props.type}-heading`}>{props.heading}</h2>
        <p>{props.children}</p>
        <div className="modal__actions">
          <Button cssClass='btn btn-primary' label='OK' onClick={props.onClose}/>
          {/* <button type="button" onClick={props.onClose} className="btn">
            Okay
          </button> */}
        </div>
      </div>
    </React.Fragment>
  );
};

export default Modal;
